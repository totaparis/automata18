# Automata 2018 AFN A AFD  UMG
Autor: Rodrigo 

[Demo AFN a AFD](https://automata-afn-to-afd.s3.amazonaws.com/index.html)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
